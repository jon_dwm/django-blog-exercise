from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.template import loader
from blog.models import User
import hashlib

def index(request):
  return render(request, 'blog/index.html')


def register_success(request):
  username = request.POST['username']
  password = request.POST['password']
  hashed_pass = hashlib.sha512(password.encode('utf-8')).hexdigest()

  try:
    User.objects.get(username=username)
    return HttpResponse("<h1>Username already exist<h1>")
  except User.DoesNotExist:
    new_user = User(password=hashed_pass, username=username)
    new_user.save()
    return HttpResponse("<h1>User registered successfully!<h1>")


def sign_in_success(request):
  username = request.POST['username']
  password = request.POST['password']
  hashed_pass = hashlib.sha512(password.encode('utf-8')).hexdigest()

  try:
    user = User.objects.get(username=username, password=hashed_pass)
    return HttpResponse("<h1>Welcome "+ user.username +", sign-in success!<h1>")
  except User.DoesNotExist:
    return HttpResponse("<h1>Wrong username or password<h1>")


def sign_in(request):
  return render(request, 'blog/sign_in.html')


def sign_up(request):
  return render(request, 'blog/sign_up.html')