from django.urls import path
from . import views

app_name = 'blog'
urlpatterns = [
  path('', views.index, name='index'),
  path('sign-in', views.sign_in, name='sign_in'),
  path('sign-up', views.sign_up, name='sign_up'),
  path('register-success', views.register_success, name='register_success'),
  path('sign-in-success', views.sign_in_success, name='sign_in_success')
]